#shader vertex
#version 450 core
layout(location = 0) in vec3 planePos;
out vec2 vUVs;
void main() {
	vUVs = vec2(0.5) + planePos.xy * 0.5;
	gl_Position = vec4(planePos, 1);
}

#shader fragment
#version 450 core

in vec2 vUVs;
uniform sampler2D fboPos;
uniform sampler2D fboColor;

layout(location = 0)out vec4 glFragColor;

void main() {
	vec3 coord = texture(fboPos, vUVs).xyz;
	vec3 color = texture(fboColor, vUVs).rgb;
	// Compute anti-aliased world-space grid lines
	vec3 grid = abs(fract(coord - 0.5) - 0.5) / fwidth(coord);
	float line = min(min(grid.x, grid.y), grid.z);
	// Just visualize the grid lines directly
	glFragColor = vec4(color, 1.0);
}
