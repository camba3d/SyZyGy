#shader vertex
#version 440 core

layout(location = 0) in vec3 meshPos;
layout(location = 3) in vec2 meshTexCoord;

out vec2 vTexCoords;

uniform mat4 matMVP;

void main() {
	gl_Position = matMVP * vec4(meshPos, 1);
    vTexCoords = meshTexCoord;
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader geometry
#version 440 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec2 vTexCoords[];

out vec2 gTexCoords;

uniform int _time; // <-- glutGet(GLUT_ELAPSED_TIME)
uniform float Magnitude = 0.001;

void main() {
    // Normal
    vec4 AB = gl_in[0].gl_Position - gl_in[1].gl_Position;
    vec4 CB = gl_in[2].gl_Position - gl_in[1].gl_Position;
    vec4 N = normalize(vec4( cross(AB.xyz, CB.xyz), 0 ));

    // Triangles
    for (uint idx = 0; idx < 3; idx++) {
        vec4 explodeDir = N * ((sin(_time * Magnitude) + 1.0) * 0.25);
        gl_Position = gl_in[idx].gl_Position + vec4(explodeDir.xyz,0);
        gTexCoords = vTexCoords[idx];
        EmitVertex();
    }
    EndPrimitive();
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core

in vec2 gTexCoords;
layout(location = 0) out vec4 glFragColor;
uniform sampler2D texDiffuse0;

void main() {
    glFragColor = vec4(1,1,0,1);
    // glFragColor = vec4(texture(texDiffuse0,gTexCoords).rgb,1);
}
