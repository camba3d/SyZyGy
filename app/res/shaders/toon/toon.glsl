#shader vertex
#version 440 core

layout(location = 0) in vec3 meshPos;
layout(location = 1) in vec3 meshNormal;
layout(location = 3) in vec2 meshTexCoord;

out vec2 vTexCoord;
out vec3 vPos, vNorm;
uniform mat4 matN, matMV, matMVP;

void main() {
	vTexCoord = meshTexCoord;
	vPos = (matMV * vec4(meshPos, 1)).xyz;
	vNorm = (matN * vec4(meshNormal, 0)).xyz;
	gl_Position = matMVP * vec4(meshPos, 1);
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader fragment
#version 440 core

in vec3 vPos;
in vec3 vNorm;
in vec2 vTexCoord;
layout(location = 0) out vec4 glFragColor;

vec3 N, V, Kd;
float Roughness;
uniform mat4 matV;
uniform sampler2D texDiffuse0;
uniform sampler2D turbulenceTex;

uniform float uToonActive;
uniform float uRoughnessFactor;
uniform float uSpecularFactor;
uniform float uDiffuseFactor;


// Lights
struct LightData {
	vec4 position;
	vec4 color;
	vec3 attenuation;
	float angle;
	vec3 direction;
	float intensity;
};
layout(binding = 0) uniform _LIGHTS{
	float Ia;
	int nLights;
	LightData LIGHTS[10];
};
vec3 toon(in vec3 color, in float toonFactor) {
	toonFactor += 1;
	return floor(color * toonFactor) / toonFactor;
}
vec3 light(in LightData _light) {
	// Light data
	vec3 L = normalize((matV * _light.position).xyz - vPos);
	float LD = length(L); // For attenuation
	vec3 H = normalize(L + V); // For blinn phong
	float I = _light.intensity;
	vec3 C = _light.color.rgb;
	// Attenuation
	vec3 attF = _light.attenuation;
	float A = 1.0 / max((attF.x + attF.y*LD + attF.z*LD*LD), 1.0);
	// Object color
	vec3 diffuse = clamp(Kd * dot(N, L), 0, 1);
	vec3 specular = vec3(1.) * pow(max(dot(N, H), 0.0001), Roughness);
	// Contribution of light to the object
	if (uToonActive == 1.) {
		diffuse = toon(diffuse, uSpecularFactor);
		specular = toon(specular, uDiffuseFactor);
	}
	return I * C * A * (diffuse + specular);
}

// EntryPoint
void main() {
	// 3D model properties
	Roughness = uRoughnessFactor;
	V = normalize(-vPos);
	N = normalize(vNorm);
	Kd = texture(turbulenceTex, vTexCoord).rgb;
	// Shade
	vec3 shade = vec3(0.85) * Kd;
	for (int i = 0; i < nLights; i++) { shade += light(LIGHTS[i]); }
	// Output
	glFragColor = vec4(shade, 1.);
}
