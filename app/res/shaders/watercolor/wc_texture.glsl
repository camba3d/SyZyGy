#shader vertex
#version 450 core
out vec2 uv;
uniform mat4 matMVP;
layout(location = 0) in vec3 meshPos;
layout(location = 1) in vec3 meshNormal;
layout(location = 3) in vec2 meshTexCoord;
void main() { uv = meshTexCoord; gl_Position = matMVP * vec4(meshPos, 1); }

// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---

#shader fragment
#version 450 core
in vec2 uv;
uniform sampler2D baseTex;
layout(location = 0) out vec4 glFragColor;
void main() { glFragColor = vec4(texture(baseTex, uv).rgb, 1.); }
