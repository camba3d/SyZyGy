#shader vertex
#version 450 core
out vec2 uvs;
layout(location = 0) in vec3 planePos;
void main() {
	uvs = vec2(0.5) + planePos.xy * 0.5;
	gl_Position = vec4(planePos, 1);
}

// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- //

#shader fragment
#version 450 core

in vec2 uvs;
layout(location = 0) out vec4 glFragColor;

// Fbo textures
uniform sampler2D fboPos;
uniform sampler2D fboNorm;
uniform sampler2D fboDepth;
uniform sampler2D fboColor;

// Input textures
uniform sampler2D paperTex;
uniform sampler2D pigmentTex;
uniform sampler2D turbulenceTex;

// User settings uniforms
uniform float uDensityFactor = 1.;
uniform float uPigmentFactor = 1.;
uniform float uIntensityFactor = 1.;
uniform float uTurbulenceFactor = 1.;
uniform float uWobblingFactor = 1.;
uniform float uEdgeFactor = 1.;

// Toggle stuff uniforms
uniform float uActiveWobbling;
uniform float uActiveTurbulence;
uniform float uActivePigment;
uniform float uActiveEdges;
uniform float uActivePaper;

// Global uniforms
uniform mat4 matV;
vec2 uv;


// Helpers

/** Compute the scaled-step of a texture to move between texels.
 *
 * @param tex, the texture where calc.
 * @param offset, real step to scale.
 * @return a vec3, with Z setted to zero for use ts.zx ts.yz style.
 *
 */
vec3 texStep(in sampler2D tex, in float offset) {
	return vec3(vec2(offset) / textureSize(tex, 0), 0);
}

/** Compute grandient of a texture with given offset.
 *
 * @param tex, the texture where calc.
 * @param offset, distance used for calc grad.
 * @param @return g, variable to store gradientX, gradientY and gradient.
 *
 */
void grad(in sampler2D tex, in float offset, out vec3 g) {
	vec3 ts = texStep(tex, offset);
	vec4 l = texture(tex, uv - ts.xz);
	vec4 r = texture(tex, uv + ts.xz);
	vec4 t = texture(tex, uv + ts.zy);
	vec4 b = texture(tex, uv - ts.zy);
	g.x = length(l - r);
	g.y = length(b - t);
	g.z = g.x + g.y;
}


// EntryPoint

void main() {


	// Retrieve data from Fbo
	vec3 P = texture(fboPos, uv).rgb;
	vec3 N = normalize(texture(fboNorm, uv).rgb);
	vec3 V = normalize(-P);
	vec3 color = texture(fboColor, uv).rgb;
	//vec3 depth = texture(fboDepth, uv).rgb;

	// Retrieve data from User input
	vec3 paper = texture(paperTex, uv).rgb;
	vec3 pigment = texture(pigmentTex, uv).rgb;
	vec3 turbulence = texture(turbulenceTex, uv).rgb;

	// Wobbling
	vec3 paperGrad;
	grad(paperTex, uWobblingFactor, paperGrad);
	color = texture(fboColor, uv + paperGrad.xy).xyz;

	// Edge darkening (intensity factor)
	vec3 colorGrad;
	grad(fboColor, uEdgeFactor, colorGrad);
	float intensity = uIntensityFactor * clamp(colorGrad.z, 0, 1);
	intensity += turbulence.x * uTurbulenceFactor;
	intensity += pigment.x * uPigmentFactor;

	// Density influenced by intensity
	float density = 1. + uDensityFactor * (intensity - 0.5);

	// Color influenced by density
	color -= (color - (color*color)) * (density - 1);

	// Put over paper
	color *= paper;

	// Output
	glFragColor = vec4(color, 1);
}
