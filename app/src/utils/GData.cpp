#include "GData.hpp"

float GData::deltaTime;
bool GData::wireFrame;
bool GData::isPostprocessActive;
int GData::activeConfig;

float GData::delta(const float& input) {
	return input * deltaTime;
}
