/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once
#include <iostream>
#include <GL/glew.h>

#define GL_ASSERT(x) glErrClear(); x; glLogErr(__FILE__,__LINE__);

// Ensure that don't get fake information
static void glErrClear() { while (glGetError() != GL_NO_ERROR); }

// Catch the error enum and parse to string
static void glLogErr(const char* file, const int& line) {
#ifndef NDEBUG
	GLenum errEnum;
	const char* errStr;
	while (errEnum = glGetError()) {
		switch (errEnum) {
			case GL_INVALID_ENUM: errStr = "GL_INVALID_ENUM"; break;
			case GL_INVALID_VALUE: errStr = "GL_INVALID_VALUE"; break;
			case GL_OUT_OF_MEMORY: errStr = "GL_OUT_OF_MEMORY"; break;
			case GL_STACK_OVERFLOW: errStr = "GL_STACK_OVERFLOW"; break;
			case GL_STACK_UNDERFLOW: errStr = "GL_STACK_UNDERFLOW"; break;
			case GL_INVALID_OPERATION: errStr = "GL_INVALID_OPERATION"; break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:  errStr = "GL_INVALID_FRAMEBUFFER_OPERATION";  break;
			default: errStr = "ERROR CODE NOT RECONGNIZED !"; break;
		}
		std::cerr << "OPENGL :: " << file << " (" << line << ") :: " << errStr << " (" << errEnum << ")" << std::endl;
	}
#endif // !NDEBUG
}
